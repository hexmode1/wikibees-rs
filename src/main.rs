use clap::Parser;

mod wiki;
use wiki::Wiki;

/// Forage a wiki
#[derive(Parser, Debug)]
#[command(term_width=0)]
#[command(version, long_about = None)]
struct Args {
    /// URL for the wiki
    #[arg(short, long)]
    url: String,
}

fn main() {
    let _build_hack = include_str!("../Cargo.toml");
    let cmd = Args::parse();

    println!( "{}", cmd.url );

}
