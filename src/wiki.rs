use core::fmt;
use url::Url;
use derivative::Derivative;

#[derive(Debug, Derivative)]
#[derivative(Default(new="true"))]
pub struct Wiki {
    #[derivative(Default(value = "\"Example\".to_string()"))]
    pub name: String,
    #[derivative(Default(value = "Url::parse(\"http://example.org\").unwrap()"))]
    pub url: Url,
    #[derivative(Default(value = "Url::parse(\"http://example.org/api\").unwrap()"))]
    api_url: Url,
    collect_general_data: bool,
    collect_extension_data: bool,
    collect_skin_data: bool,
    collect_logs: bool,
    collect_recent_changes: bool,
    collect_semantic_statistics: bool,
    #[derivative(Default(value = "240"))]
    check_every: u32,
    audited: bool,
    curated: bool,
    active: bool,
    demote: bool,
    defunct: bool,
}

impl fmt::Display for Wiki {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ({})", self.name, self.url)
    }
}
