enum FarmType {}

struct Farm {
    name: String,
    url: Url,
    organization: String,
    description: String,
    note: String,
    farm: String,
    farm_type: FarmType,
    image: String,
    shared_users: bool,
    multilingual: bool,
    collecting: bool
}

enum Stat {
    ApiUrl,
    SpecialPage,
}

enum Override {}

